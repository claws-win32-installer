#!/bin/bash

failed_downloads=""
failed_hash=""
skipped_hash=""

failed_download () {
	failed_downloads="$failed_downloads $name"
	printf "$@"
}

pkg_root="$(readlink -f $(dirname $0))"

download_file () {
	file=$(basename "$url")

	if [[ -f "$file" ]]; then
		return 0
	fi

	curl -# -f -L -O "$url" || {
		failed_download "Error while downloading %s package from %s\n" "$name" "$url"
		return 1
	}
}

download_git () {
	if [[ "$name" = "claws_mail" ]]; then
		echo "Will derive version from ref \"$ref\" instead of using \"$version\""
	else
		file="${name}-${version}.tar.xz"

		if [[ -f "$file" ]]; then
			return 0
		fi
	fi

	# Clone the repo if it doesn't exist
	if [[ ! -d "$name" ]]; then
		if [[ -n "$branch" ]]; then
			git clone --single-branch --branch "$branch" "$url" "$name" || {
				failed_download "Error cloning %s --single-branch=%s\n" "$name" "$branch"
				return 1
			}
		else
			git clone "$url" "$name" || {
				failed_download "Error cloning %s\n" "$name"
				return 1
			}
		fi
	fi

	pushd "$name" > /dev/null || exit 1

	# Check if the ref already exists.  Attempt to pull if it doesn't
	git rev-parse -q --verify "$ref^{commit}" || {
		git pull || {
			failed_download "Error pulling %s\n" "$name"
			popd > /dev/null
			return 1
		}

		git rev-parse -q --verify "$ref^{commit}" || {
			failed_download "Did not find ref %s in repo %s after pulling\n" "$ref" "$url"
			popd > /dev/null
			return 1
		}
	}

	# Create the archive from the ref
	if [[ "$name" = "claws_mail" ]]; then
		# Special handling for Claws
		git_ver=$(git describe --abbrev=6 --always "$ref")
		file="${name}-${git_ver}.tar.xz"
		sed -i "s/^claws_mail,.*,git,/claws_mail,${git_ver},git,/" ../packages.current

		if [[ ! -f "../${file}" ]]; then
			echo "echo $git_ver" > version
			git archive --format=tar --prefix="${name}-${git_ver}/" --add-file=version "$ref" | xz -z > "../${file}"
			rm version
		fi
	else
		git archive --format=tar --prefix="${name}-${version}/" "$ref" | xz -z > "../${file}"
	fi

	popd > /dev/null
	return 0
}

gen_cert_bundle () {
	printf "%s" "$failed_downloads" | grep -qw "certdata"
	if [[ $? -eq 0 ]]; then
		printf "\nError: Failed to download CA certificate data\n"
		return 1
	fi

	printf "%s" "$failed_hash" | grep -qw "certdata"
	if [[ $? -eq 0 ]]; then
		printf "\nError: SHA256 verification failed for CA certificate data\n"
		return 1
	fi

	printf "%s" "$skipped_hash" | grep -qw "certdata"
	if [[ $? -eq 0 ]]; then
		printf "\nError: SHA256 verification skipped for CA certificate data\n"
		return 1
	fi

	./mk-ca-bundle.pl -f -n ca-certificates.crt
}

pushd "${pkg_root}" > /dev/null || exit 1
ret=0
current=1
total=$(sed -e '/^#/d' -e '/^$/d' packages.current | wc -l)

while IFS=, read name version type url hash branch ref
do
	printf "\033[1mpackage %02d/%d %s %s\033[0m\n" "$current" "$total" "$name" "$version"
	if [[ "$type" = "file" ]]; then
		download_file || continue
	elif [[ "$type" = "git" ]]; then
		download_git || continue
	else
		printf "Bad package type %s for package %s\n" "$type" "$name"
		exit 1
	fi

	if [[ -z "$hash" ]]; then
		printf "No hash specified - Skipping sha256sum for %s package\n" "$name"
		skipped_hash="$skipped_hash $name"
	else
		printf "%s *%s" "$hash" "$file" | sha256sum -c
		if [[ $? != 0 ]]; then
			printf "Error while checking sha256sum for %s package\n" "$name"
			failed_hash="$failed_hash $name"
		fi
	fi

	(( current++ ))

done < <(sed -e '/^#/d' -e '/^$/d' packages.current)

# Generate the ca-certificates.crt bundle
gen_cert_bundle
if [[ $? -ne 0 ]]; then
	printf "\n\033[31mError:\033[0m Failed to generate CA certificate bundle\n"
	ret=1
fi

if [[ -n "$failed_downloads" ]]; then
	printf "\n\033[31mError:\033[0m Failed to download these packages: %s\n" "$failed_downloads"
	ret=1
fi

if [[ -n "$failed_hash" ]]; then
	printf "\n\033[31mError:\033[0m SHA256 verification failed for these packages: %s\n" "$failed_hash"
	ret=1
fi

if [[ -n "$skipped_hash" ]]; then
	printf "\n\033[33mWarning:\033[0m SHA256 verification skipped for these packages: %s\n" "$skipped_hash"
	ret=1
fi

popd > /dev/null
exit $ret

