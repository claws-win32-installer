### Unconditionally uninstall a previous installation if found
Section "-remove_previous"
	StrCmp $OLD_UNINSTALLER "" gpg2

	DetailPrint "Uninstalling previous version"
	ExecWait "$OLD_UNINSTALLER /S _?=$OLD_INSTDIR"

	# Need to strip the quotes from $OLD_UNINSTALLER for Delete
	StrCpy $R0 $OLD_UNINSTALLER 1
	StrCmp $R0 `"` 0 +2
	StrCpy $OLD_UNINSTALLER $OLD_UNINSTALLER `` 1
	StrCpy $R0 $OLD_UNINSTALLER 1 -1
	StrCmp $R0 `"` 0 +2
	StrCpy $OLD_UNINSTALLER $OLD_UNINSTALLER -1
	Delete "$OLD_UNINSTALLER"
	RMDir "$OLD_INSTDIR"

	gpg2:

### uninstall gnupg2, gpa and its dependencies
### Uninstaller needs to have a similar section, doing the same work.
!include "gnupg-uninst.nsi"

SectionEnd


### Start of the installer section
${MementoSection} "-Claws-Mail" SEC_claws_mail

SetOutPath "$INSTDIR"

# Install the gcc and mingw32 runtime libraries
File "${gcc_dir}/${libstdcpp_dll}"
File "${gcc_dir}/${libgcc_dll}"
File "${libwinpthread_dir}/${libwinpthread_dll}"

### Now install files for each software component

#######################################
### libiconv
!insertmacro SetPrefix libiconv
File ${prefix}/bin/libiconv-2.dll

#######################################
### libtasn1
!insertmacro SetPrefix libtasn1
File ${prefix}/bin/libtasn1-6.dll

#######################################
### regex
!insertmacro SetPrefix regex
File ${prefix}/bin/libregex-1.dll

#######################################
### zlib
!insertmacro SetPrefix zlib
File ${prefix}/bin/zlib1.dll

#######################################
### bsfilter
!insertmacro SetPrefix bsfilter
File ${prefix}/bin/bsfilterw.exe
File ${prefix}/bin/bsfilter.exe
File ${prefix}/bin/iconv.dll

#######################################
### gettext
!insertmacro SetPrefix gettext
File ${prefix}/bin/libintl-8.dll

#######################################
### libpng
!insertmacro SetPrefix libpng
File ${prefix}/bin/libpng16-16.dll

#######################################
### hunspell
!insertmacro SetPrefix hunspell
File ${prefix}/bin/libhunspell-1.7-0.dll

#######################################
### enchant
!insertmacro SetPrefix enchant
File ${prefix}/bin/libenchant-2-2.dll
SetOutPath "$INSTDIR\lib\enchant-2"
File ${prefix}/lib/enchant-2/enchant_hunspell.dll

SetShellVarContext all
SetOutPath "$INSTDIR\share\enchant-2"
File ${prefix}/share/enchant-2/enchant.ordering
SetOutPath "$INSTDIR\share\hunspell"
File dictionaries/en_US.aff
File dictionaries/en_US.dic
File dictionaries/de_DE_frami.aff
File dictionaries/de_DE_frami.dic
File dictionaries/fr.aff
File dictionaries/fr.dic
SetOutPath "$INSTDIR"

#######################################
### cyrus-sasl
!insertmacro SetPrefix cyrus_sasl
File ${prefix}/bin/libsasl2-3.dll
SetOutPath "$INSTDIR\lib\sasl2"
File ${prefix}/lib/sasl2/libanonymous-3.dll
File ${prefix}/lib/sasl2/libcrammd5-3.dll
File ${prefix}/lib/sasl2/libdigestmd5-3.dll
File ${prefix}/lib/sasl2/liblogin-3.dll
File ${prefix}/lib/sasl2/libplain-3.dll
SetOutPath "$INSTDIR"

#######################################
### libetpan
!insertmacro SetPrefix libetpan
File ${prefix}/bin/libetpan-20.dll

#######################################
### libassuan
!insertmacro SetPrefix libassuan
File ${prefix}/bin/libassuan-9.dll

#######################################
### libgpg-error
!insertmacro SetPrefix libgpg_error
File ${prefix}/bin/gpg-error.exe
File ${prefix}/bin/libgpg-error-0.dll

SetOutPath "$INSTDIR\share\locale\"
File /r ${prefix}/share/locale/
SetOutPath "$INSTDIR"

#######################################
### libffi
!insertmacro SetPrefix libffi
File ${prefix}/bin/libffi-8.dll

#######################################
### pcre2
!insertmacro SetPrefix pcre2
File ${prefix}/bin/libpcre2-8-0.dll

#######################################
### glib
!insertmacro SetPrefix glib
File ${prefix}/bin/libglib-2.0-0.dll
File ${prefix}/bin/libgmodule-2.0-0.dll
File ${prefix}/bin/libgobject-2.0-0.dll
File ${prefix}/bin/libgthread-2.0-0.dll
File ${prefix}/bin/libgio-2.0-0.dll

File ${prefix}/bin/gdbus.exe
File ${prefix}/bin/gio-querymodules.exe
File ${prefix}/bin/glib-compile-resources.exe
File ${prefix}/bin/glib-compile-schemas.exe
File ${prefix}/bin/gobject-query.exe
File ${prefix}/bin/gresource.exe
File ${prefix}/bin/gsettings.exe
!if ${nsis64} == "yes"
File ${prefix}/bin/gspawn-win64-helper.exe
File ${prefix}/bin/gspawn-win64-helper-console.exe
!else
File ${prefix}/bin/gspawn-win32-helper.exe
File ${prefix}/bin/gspawn-win32-helper-console.exe
!endif

SetOutPath "$INSTDIR\share\glib-2.0\schemas"
File ${prefix}/share/glib-2.0/schemas/gschema.dtd

SetOutPath "$INSTDIR\share\locale\"
File /r ${prefix}/share/locale/
SetOutPath "$INSTDIR"

#######################################
### expat
!insertmacro SetPrefix expat
File ${prefix}/bin/libexpat-1.dll

#######################################
### freetype
!insertmacro SetPrefix freetype
File ${prefix}/bin/libfreetype-6.dll

#######################################
### fontconfig
!insertmacro SetPrefix fontconfig
SetOutPath "$INSTDIR\etc\fonts"
File ${prefix}/etc/fonts/fonts.conf

SetOutPath "$INSTDIR"
File ${prefix}/bin/libfontconfig-1.dll

#######################################
### pixman
!insertmacro SetPrefix pixman
File ${prefix}/bin/libpixman-1-0.dll

#######################################
### cairo
!insertmacro SetPrefix cairo
File ${prefix}/bin/libcairo-2.dll
File ${prefix}/bin/libcairo-gobject-2.dll
File ${prefix}/bin/libcairo-script-interpreter-2.dll

#######################################
### harfbuzz
!insertmacro SetPrefix harfbuzz
File ${prefix}/bin/libharfbuzz-0.dll
File ${prefix}/bin/libharfbuzz-icu-0.dll

#######################################
### fribidi
!insertmacro SetPrefix fribidi
File ${prefix}/bin/libfribidi-0.dll

#######################################
### pango
!insertmacro SetPrefix pango
File ${prefix}/bin/libpango-1.0-0.dll
File ${prefix}/bin/libpangoft2-1.0-0.dll
File ${prefix}/bin/libpangowin32-1.0-0.dll
File ${prefix}/bin/libpangocairo-1.0-0.dll

#######################################
### atk
!insertmacro SetPrefix atk
File ${prefix}/bin/libatk-1.0-0.dll

SetOutPath "$INSTDIR\share\locale\"
File /r ${prefix}/share/locale/
SetOutPath "$INSTDIR"

#######################################
### gdk-pixbuf
!insertmacro SetPrefix gdk_pixbuf
File ${prefix}/bin/libgdk_pixbuf-2.0-0.dll
File ${prefix}/bin/gdk-pixbuf-query-loaders.exe
File ${prefix}/bin/gdk-pixbuf-pixdata.exe

#######################################
### gtk+
!insertmacro SetPrefix gtk
File ${prefix}/bin/libgailutil-3-0.dll
File ${prefix}/bin/libgdk-3-0.dll
File ${prefix}/bin/libgtk-3-0.dll
File ${prefix}/bin/gtk-query-immodules-3.0.exe
File ${prefix}/bin/gtk-update-icon-cache.exe

SetOutPath "$INSTDIR\etc\gtk-3.0"
File ${prefix}/etc/gtk-3.0/im-multipress.conf

SetOutPath "$INSTDIR\share\themes\Default\gtk-3.0"
File ${prefix}/share/themes/Default/gtk-3.0/gtk-keys.css

SetOutPath "$INSTDIR\share\themes\Emacs\gtk-3.0"
File ${prefix}/share/themes/Emacs/gtk-3.0/gtk-keys.css

SetOutPath "$INSTDIR\share\locale\"
File /r ${prefix}/share/locale/

SetOutPath "$INSTDIR\share\glib-2.0\schemas"
File ${prefix}/share/glib-2.0/schemas/org.gtk.Settings.ColorChooser.gschema.xml
File ${prefix}/share/glib-2.0/schemas/org.gtk.Settings.Debug.gschema.xml
File ${prefix}/share/glib-2.0/schemas/org.gtk.Settings.EmojiChooser.gschema.xml
File ${prefix}/share/glib-2.0/schemas/org.gtk.Settings.FileChooser.gschema.xml
ExecShell "open" "$INSTDIR\glib-compile-schemas.exe" '"$INSTDIR\share\glib-2.0\schemas"' SW_HIDE
SetOutPath "$INSTDIR"

#######################################
### adwaita-icon-theme
!insertmacro SetPrefix adwaita_icon_theme
SetOutPath "$INSTDIR\share\icons\Adwaita\16x16\actions"
File ${prefix}/share/icons/Adwaita/16x16/actions/bookmark-new-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/document-save-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/find-location-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/folder-new-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/list-add-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/list-remove-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/media-eject-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/media-playback-pause-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/media-record-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/object-select-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/tab-new-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/actions/view-list-symbolic.symbolic.png

SetOutPath "$INSTDIR\share\icons\Adwaita\16x16\categories"
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-activities-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-body-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-flags-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-food-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-nature-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-objects-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-people-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-recent-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-symbols-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/categories/emoji-travel-symbolic.symbolic.png

SetOutPath "$INSTDIR\share\icons\Adwaita\16x16\devices"
File ${prefix}/share/icons/Adwaita/16x16/devices/drive-harddisk-symbolic.symbolic.png

SetOutPath "$INSTDIR\share\icons\Adwaita\16x16\legacy"
File ${prefix}/share/icons/Adwaita/16x16/legacy/dialog-error.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/dialog-information.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/dialog-password.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/dialog-question.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/dialog-warning.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/document-properties.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/edit-clear.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/edit-copy.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/edit-delete.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/edit-find.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/edit-redo.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/edit-undo.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/go-bottom.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/go-down.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/go-next.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/go-previous.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/go-top.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/go-up.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/help-browser.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/image-missing.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/list-add.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/list-remove.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/system-help.png
File ${prefix}/share/icons/Adwaita/16x16/legacy/view-refresh.png

SetOutPath "$INSTDIR\share\icons\Adwaita\16x16\places"
File ${prefix}/share/icons/Adwaita/16x16/places/folder.png
File ${prefix}/share/icons/Adwaita/16x16/places/user-desktop-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/places/user-home-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/places/user-trash-symbolic.symbolic.png

SetOutPath "$INSTDIR\share\icons\Adwaita\16x16\ui"
File ${prefix}/share/icons/Adwaita/16x16/ui/checkbox-checked-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/checkbox-mixed-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/checkbox-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/focus-legacy-systray-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/focus-top-bar-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/focus-windows-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/list-drag-handle-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/pan-down-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/pan-end-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/pan-end-symbolic-rtl.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/pan-start-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/pan-start-symbolic-rtl.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/pan-up-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/radio-checked-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/radio-mixed-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/radio-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/selection-end-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/selection-end-symbolic-rtl.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/selection-start-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/selection-start-symbolic-rtl.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/window-close-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/window-maximize-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/window-minimize-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/window-new-symbolic.symbolic.png
File ${prefix}/share/icons/Adwaita/16x16/ui/window-restore-symbolic.symbolic.png

SetOutPath "$INSTDIR\share\icons\Adwaita\32x32\mimetypes"
File ${prefix}/share/icons/Adwaita/32x32/mimetypes/text-x-generic.png

SetOutPath "$INSTDIR\share\icons\Adwaita\48x48\legacy"
File ${prefix}/share/icons/Adwaita/48x48/legacy/dialog-error.png
File ${prefix}/share/icons/Adwaita/48x48/legacy/dialog-information.png
File ${prefix}/share/icons/Adwaita/48x48/legacy/dialog-password.png
File ${prefix}/share/icons/Adwaita/48x48/legacy/dialog-question.png
File ${prefix}/share/icons/Adwaita/48x48/legacy/dialog-warning.png

SetOutPath "$INSTDIR\share\icons\Adwaita\256x256\legacy"
File ${prefix}/share/icons/Adwaita/256x256/legacy/dialog-error.png
File ${prefix}/share/icons/Adwaita/256x256/legacy/dialog-information.png
File ${prefix}/share/icons/Adwaita/256x256/legacy/dialog-password.png
File ${prefix}/share/icons/Adwaita/256x256/legacy/dialog-question.png
File ${prefix}/share/icons/Adwaita/256x256/legacy/dialog-warning.png

SetOutPath "$INSTDIR\share\icons\Adwaita\cursors"
File ${prefix}/share/icons/Adwaita/cursors/dnd-copy.cur
File ${prefix}/share/icons/Adwaita/cursors/dnd-move.cur
File ${prefix}/share/icons/Adwaita/cursors/dnd-none.cur

SetOutPath "$INSTDIR\share\icons\Adwaita"
File ${prefix}/share/icons/Adwaita/index.theme

ExecShell "open" "$INSTDIR\gtk-update-icon-cache.exe" '-q -t -f "$INSTDIR\share\icons\Adwaita"' SW_HIDE
SetOutPath "$INSTDIR"

#######################################
### gtk+ win71 theme
!insertmacro SetPrefix gtk
SetOutPath "$INSTDIR\etc\gtk-3.0"
File ${prefix}/share/themes/claws/settings.ini
SetOutPath "$INSTDIR\share\themes\win71\gtk-3.0"
File ${prefix}/share/themes/claws/win71/gtk-win32-base.css
File ${Prefix}/share/themes/claws/win71/gtk.css
SetOutPath "$INSTDIR"

#######################################
### p11-kit
!insertmacro SetPrefix p11_kit
File ${prefix}/bin/libp11-kit-0.dll

#######################################
### gnutls
!insertmacro SetPrefix gnutls
File ${prefix}/bin/libgnutls-30.dll

#######################################
### curl
!insertmacro SetPrefix curl
File ${prefix}/bin/libcurl-4.dll

#######################################
### libxml2
!insertmacro SetPrefix libxml2
File ${prefix}/bin/libxml2-2.dll

#######################################
### gpgme
!insertmacro SetPrefix gpgme
File ${prefix}/bin/libgpgme-11.dll
File ${prefix}/bin/libgpgmepp-6.dll
File ${prefix}/bin/libgpgme-glib-11.dll
File ${prefix}/libexec/gpgme-w32spawn.exe

#######################################
### icu4c
!insertmacro SetPrefix icu4c
File ${prefix}/bin/icudt76.dll
File ${prefix}/bin/icuin76.dll
File ${prefix}/bin/icuuc76.dll

#######################################
### libepoxy
!insertmacro SetPrefix libepoxy
SetOutPath "$INSTDIR"
File ${prefix}/bin/libepoxy-0.dll

#######################################
### jpeg
!insertmacro SetPrefix jpeg
File ${prefix}/bin/libjpeg-9.dll

#######################################
### libpsl
!insertmacro SetPrefix libpsl
File ${prefix}/bin/libpsl-5.dll

#######################################
### gmp
!insertmacro SetPrefix gmp
File ${prefix}/bin/libgmp-10.dll

#######################################
### nettle
!insertmacro SetPrefix nettle
File ${prefix}/bin/libnettle-8.dll
File ${prefix}/bin/libhogweed-6.dll

#######################################
### libical
!insertmacro SetPrefix libical
File ${prefix}/bin/libical.dll
File ${prefix}/bin/libicalss.dll
File ${prefix}/bin/libicalvcal.dll

#######################################
### ytnef
!insertmacro SetPrefix ytnef
File ${prefix}/bin/libytnef-0.dll

#######################################
### gumbo-parser
!insertmacro SetPrefix gumbo_parser
File ${prefix}/bin/libgumbo-3.dll

#######################################
### poppler
!insertmacro SetPrefix poppler
File ${prefix}/bin/libpoppler-glib-8.dll
File ${prefix}/bin/libpoppler-146.dll

#######################################
### claws-mail
!insertmacro SetPrefix claws_mail
File ${prefix}/bin/claws-mail.exe
File ${prefix}/share/doc/claws-mail/manual/en/claws-mail-manual.pdf

SetOutPath "$INSTDIR\share\claws-mail"
File ${TOP_SRCDIR}/packages/ca-certificates.crt

SetOutPath "$INSTDIR\lib\claws-mail\plugins"
File ${prefix}/lib/claws-mail/plugins/address_keeper.dll
File ${prefix}/lib/claws-mail/plugins/attachwarner.dll
File ${prefix}/lib/claws-mail/plugins/att_remover.dll
File ${prefix}/lib/claws-mail/plugins/bsfilter.dll
File ${prefix}/lib/claws-mail/plugins/fetchinfo.dll
File ${prefix}/lib/claws-mail/plugins/keyword_warner.dll
File ${prefix}/lib/claws-mail/plugins/libravatar.dll
File ${prefix}/lib/claws-mail/plugins/litehtml_viewer.dll
File ${prefix}/lib/claws-mail/plugins/managesieve.dll
File ${prefix}/lib/claws-mail/plugins/notification.dll
File ${prefix}/lib/claws-mail/plugins/pdf_viewer.dll
File ${prefix}/lib/claws-mail/plugins/pgpcore.dll
File ${prefix}/lib/claws-mail/plugins/pgpinline.dll
File ${prefix}/lib/claws-mail/plugins/pgpinline.deps
File ${prefix}/lib/claws-mail/plugins/pgpmime.dll
File ${prefix}/lib/claws-mail/plugins/pgpmime.deps
File ${prefix}/lib/claws-mail/plugins/rssyl.dll
File ${prefix}/lib/claws-mail/plugins/smime.dll
File ${prefix}/lib/claws-mail/plugins/smime.deps
File ${prefix}/lib/claws-mail/plugins/spamreport.dll
File ${prefix}/lib/claws-mail/plugins/tnef_parse.dll
File ${prefix}/lib/claws-mail/plugins/vcalendar.dll

SetOutPath "$INSTDIR\share\locale\"
File /r ${prefix}/share/locale/
SetOutPath "$INSTDIR"

DetailPrint "Writing VERSION file"
FileOpen $0 "$INSTDIR\VERSION" w
FileWrite $0 "${PACKAGE}$\r$\n"
FileWrite $0 "${VERSION}$\r$\n"
FileClose $0

${MementoSectionEnd}

${MementoSectionDone}

##############################################
### Windows Registry modification section
Section

!insertmacro MUI_INSTALLOPTIONS_READ $R0 "installer-setdefaultclient.ini" \
"Field 1" "State"
IntCmp $R0 0 skip_default_client

WriteRegStr   HKCU "SOFTWARE\Classes\mailto" "" "URL:MailTo-Protocol"
WriteRegStr   HKCU "SOFTWARE\Classes\mailto" "URL Protocol" ""
WriteRegDword HKCU "SOFTWARE\Classes\mailto" "EditFlags" 2
WriteRegStr   HKCU "SOFTWARE\Classes\mailto" "FriendlyTypeName" "Claws-Mail URL"
WriteRegStr   HKCU "SOFTWARE\Classes\mailto\DefaultIcon" "" "$INSTDIR\claws-mail.exe,0"
WriteRegStr   HKCU "SOFTWARE\Classes\mailto\shell\open\command" "" "$INSTDIR\claws-mail.exe --compose %1"
DetailPrint "Set Claws Mail as default mail client"

skip_default_client:
#just register Claws in the list of available mailers
WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail" "" "Claws Mail"
WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail" "DLLPath" ""

WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Capabilities" "ApplicationDescription" "Fast, lightweight and user-friendly GTK+3 based email client"
WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Capabilities" "ApplicationIcon" "$INSTDIR\claws-mail.exe,0"
WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Capabilities" "ApplicationName" "Claws Mail"

WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Capabilities\UrlAssociations" "mailto" "Claws-Mail.URL.mailto"

WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Protocols\mailto" "" "URL:MailTo-Protocol"
WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Protocols\mailto" "URL Protocol" ""
WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Protocols\mailto" "FriendlyTypeName" "Claws-Mail URL"
WriteRegDword HKLM "SOFTWARE\Clients\Mail\Claws Mail\Protocols\mailto" "EditFlags" 2
WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Protocols\mailto\DefaultIcon" "" "$INSTDIR\claws-mail.exe,0"
WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\Protocols\mailto\shell\open\command" "" "$INSTDIR\claws-mail.exe --compose %1"

WriteRegStr   HKLM "SOFTWARE\Clients\Mail\Claws Mail\shell\open\command" "" "$INSTDIR\claws-mail.exe"

WriteRegStr   HKLM "SOFTWARE\Classes\Claws-Mail.URL.mailto" "" "Claws-Mail URL"
WriteRegStr   HKLM "SOFTWARE\Classes\Claws-Mail.URL.mailto" "FriendlyTypeName" "Claws-Mail URL"
WriteRegStr   HKLM "SOFTWARE\Classes\Claws-Mail.URL.mailto" "URL Protocol" ""
WriteRegDword HKLM "SOFTWARE\Classes\Claws-Mail.URL.mailto" "EditFlags" 2

WriteRegStr   HKLM "SOFTWARE\Classes\Claws-Mail.URL.mailto\DefaultIcon" "" "$INSTDIR\claws-mail.exe,0"
WriteRegStr   HKLM "SOFTWARE\Classes\Claws-Mail.URL.mailto\shell\open\command" "" "$INSTDIR\claws-mail.exe --compose %1"

# Windows 8
WriteRegStr HKLM "SOFTWARE\RegisteredApplications" "Claws Mail" "Software\Clients\Mail\Claws Mail\Capabilities"

# Windows Add/Remove Programs support
Var /GLOBAL MYTMP
StrCpy $MYTMP "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRETTY_PACKAGE_SHORT}"
WriteRegExpandStr HKLM $MYTMP "UninstallString" '"$INSTDIR\claws-mail-uninstall.exe"'
WriteRegExpandStr HKLM $MYTMP "InstallLocation" "$INSTDIR"
WriteRegStr       HKLM $MYTMP "DisplayName"     "Claws Mail"
WriteRegStr       HKLM $MYTMP "DisplayIcon"     "$INSTDIR\claws-mail.exe,0"
WriteRegStr       HKLM $MYTMP "DisplayVersion"  "${VERSION}"
WriteRegStr       HKLM $MYTMP "Publisher"       "${COMPANY}"
WriteRegStr       HKLM $MYTMP "URLInfoAbout"    "${WEBSITE}"
WriteRegDWORD     HKLM $MYTMP "NoModify"        "1"
WriteRegDWORD     HKLM $MYTMP "NoRepair"        "1"
DetailPrint "Added Claws Mail info to Add/Remove Programs list"

SectionEnd

### Create the uninstaller and shortcuts
Section
WriteUninstaller "$INSTDIR\claws-mail-uninstall.exe"

# Delete the old stuff, also old names of previous versions.
Delete "$DESKTOP\Claws-Mail.lnk"
Delete "$DESKTOP\Claws-Mail Manual.lnk"

# Start Menu
!insertmacro MUI_INSTALLOPTIONS_READ $R0 "installer-options.ini" \
	"Field 2" "State"
	IntCmp $R0 0 no_start_menu

	SetOutPath "$INSTDIR"

	CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"

	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Claws-Mail.lnk" \
		"$INSTDIR\claws-mail.exe" \
		"" "" "" SW_SHOWNORMAL "" $(T_Menu_ClawsMail)

	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Claws-Mail Manual.lnk" \
		"$INSTDIR\claws-mail-manual.pdf" \
		"" "" "" SW_SHOWNORMAL "" $(T_Menu_ClawsMailManual)

	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" \
		"$INSTDIR\claws-mail-uninstall.exe" \
		"" "$INSTDIR\claws-mail-uninstall.exe" "" SW_SHOWNORMAL "" $(T_Menu_Uninstall)

	no_start_menu:

# Desktop
!insertmacro MUI_INSTALLOPTIONS_READ $R0 "installer-options.ini" \
	"Field 3" "State"
	IntCmp $R0 0 no_desktop

	SetOutPath "$INSTDIR"

	CreateShortCut "$DESKTOP\Claws-Mail.lnk" \
		"$INSTDIR\claws-mail.exe" \
		"" "" "" SW_SHOWNORMAL "" $(T_Menu_ClawsMail)
	
	no_desktop:

# Quick Launch
!insertmacro MUI_INSTALLOPTIONS_READ $R0 "installer-options.ini" \
	"Field 4" "State"
	IntCmp $R0 0 no_quicklaunch

	SetOutPath "$INSTDIR"

	CreateShortCut "$QUICKLAUNCH\Claws-Mail.lnk" \
		"$INSTDIR\claws-mail.exe" \
		"" "" "" SW_SHOWNORMAL "" $(T_Menu_ClawsMail)

	no_quicklaunch:

SectionEnd

