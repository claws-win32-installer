LangString T_LangCode ${LANG_ENGLISH} "en"

# Licensing
LangString T_GPLHeader ${LANG_ENGLISH} \
	"This software is licensed under the terms of the GNU General Public License (GPL)."

LangString T_GPLShort ${LANG_ENGLISH} \
	"In short: You are allowed to run this software for any purpose. \
You may distribute it as long as you give the recipients the same \
rights you have received."

# Installation options title
LangString T_InstallOptions ${LANG_ENGLISH} "Install Options"

# Installation options subtitle 1
LangString T_InstallOptLinks ${LANG_ENGLISH} "Start links"

LangString T_InstOptLabelA  ${LANG_ENGLISH} \
     "Please select where Claws Mail shall install links:"

LangString T_InstOptLabelB  ${LANG_ENGLISH} \
     "(Only programs will be linked into the quick launch bar.)"

LangString T_InstOptFieldA  ${LANG_ENGLISH} \
     "Start Menu"

LangString T_InstOptFieldB  ${LANG_ENGLISH} \
     "Desktop"

LangString T_InstOptFieldC  ${LANG_ENGLISH} \
     "Quick Launch Bar"


LangString T_MoreInfo ${LANG_ENGLISH} \
     "Click here for the project homepage"

# Strings pertaining to the existing version check
LangString T_FoundExisting ${LANG_ENGLISH} \
	"$OLD_DISPNAME $OLD_DISPVER will be uninstalled and replaced with ${PRETTY_PACKAGE} ${VERSION} \
$\r$\n$\r$\nNo user data or settings will be touched.  Do you want to continue?"

# Welcome Page strings
LangString T_WelcomePageTitle ${LANG_ENGLISH} \
	"Welcome to the installation of ${PRETTY_PACKAGE}"

# The About string as displayed on the first page.
LangString T_About ${LANG_ENGLISH} \
	"Claws Mail is an email client (and news reader), based on GTK+, featuring \r\n\
Quick response\r\n\
Graceful, and sophisticated interface\r\n\
Easy configuration, intuitive operation\r\n\
Abundant features\r\n\
Extensibility\r\n\
Robustness and stability.\r\n\
It is Free Software, released under the GNU GPL v3 or later."

LangString T_AboutVersion ${LANG_ENGLISH} \
 "This is Claws Mail version ${VERSION_NO_REL}${GIT_REVISION}-${RELEASE}"

LangString T_AboutFileVersion ${LANG_ENGLISH} \
 "File version ${PROD_VERSION}"

LangString T_AboutReleaseDate ${LANG_ENGLISH} \
 "Release date ${_BUILD_ISODATE}"

LangString T_SetDefaultClientHeader ${LANG_ENGLISH} \
"Default client"

LangString T_SetDefaultClient ${LANG_ENGLISH} \
"Do you want Claws Mail to handle 'mailto:' URLs?"

LangString T_FoundGnupg ${LANG_ENGLISH} \
"GnuPG2 was found in your previous installation directory. \
$\r$\nPlease note that GnuPG and GPA utilities are no longer \
bundled with Claws Mail and will be removed. \
If you wish to continue using these utilities, install an \
up-to-date version of Gpg4win from: \
$\r$\nhttps://www.gpg4win.org/$\r$\n$\r$\n \
Do you want to continue installing Claws Mail?"

LangString T_Menu_Uninstall ${LANG_ENGLISH} \
"Uninstall"

LangString T_Menu_ClawsMail ${LANG_ENGLISH} \
"Run Claws Mail"

LangString T_Menu_ClawsMailManual ${LANG_ENGLISH} \
"Claws Mail Manual"
